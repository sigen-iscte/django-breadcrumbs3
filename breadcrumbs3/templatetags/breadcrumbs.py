from django import template
from django.template.loader import get_template

register = template.Library()


@register.simple_tag(takes_context=True)
def breadcrumbs(context):
    """
    Provides the {% breadcrumbs %} tag to render a list of breadcrumbs.
    The rendered template :

    """
    crumbs = context['request'].breadcrumbs.__iter__()

    return get_template("breadcrumbs3/breadcrumb.html").render({'breadcrumb': crumbs})
