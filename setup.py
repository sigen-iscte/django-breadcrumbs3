from setuptools import find_packages, setup

from breadcrumbs3 import __version__

setup(
    name='django-breadcrumbs3',
    version=__version__,
    license='BSD',
    author='João Lourenço',
    author_email='jrelo@iscte.pt',
    description='Breadcrumbs app for Django',
    long_description='See https://bitbucket.org/sigen-iscte/django-breadcrumbs3 for the README',
    url='https://bitbucket.org/sigen-iscte/django-breadcrumbs3',
    install_requires=[
        'Django>=1.7',
    ],
    include_package_data=True,
    packages=find_packages(),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
